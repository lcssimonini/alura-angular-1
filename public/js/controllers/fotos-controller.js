angular.module('alurapic').controller('FotosController', function($scope, $http) {


  $scope.fotos = [];

  $http.get('v1/fotos').success(function(data) {
    $scope.fotos = data;
  }).error(function(error) {
    console.log(error);
  });


  // $http.get('v1/fotos').then(function(response) {
  //   $scope.fotos = response.data;
  // }).catch(function(error) {
  //   console.log(error);
  // });

});
